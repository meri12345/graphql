import React from 'react';
import { getContent } from "../gql/content";
import {MAIN_MENU} from "../gql/menu";
import Menu from "../components/Menu";
import { client } from '../apolloClient'
import { PageTitle } from '../components/PageTitle';
import { Node } from '../components/EntityType/Node/Node';

function Page(data) {

  return (
    <div className="page">
      <header className="content-center">
        <PageTitle />
        <Menu data={data.menu} />
      </header>
      <div className="body content-center">
        <div className="container">
          <Node node={data.entity} />
        </div>
      </div>
    </div>
  );
}

export async function getServerSideProps(context) {
  let entity = null;
  const menu = [];

  await client.query({
    fetchPolicy: 'no-cache',
    query: getContent(getUrl(context))
  })
    .then((result) => {
      entity = result.data.entityByRoute;
    });

  await client.query({
    query: MAIN_MENU
  })
    .then((result) => {
      result.data.menuByName.links.forEach((item) => {
        menu.push({
          label: item.label,
          url: item.url
        });
      });
    });

  return {
    props: {
      menu,
      entity,
    }
  };
}

// Get url without hostname
function getUrl(context) {
    return context.req.url;
}

export default Page