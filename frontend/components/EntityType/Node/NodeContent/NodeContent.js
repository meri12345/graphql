import React from 'react';
import Header from '../../../Header';
import { Paragraph } from '../../Paragraph/Paragraph';

export const NodeContent = ({ data }) => (
    <div className="container">
        <div className="content">
            <div className="content-header">
                <Header item={data.title}/>
            </div>
            <div className="content-body">
              <h3>Paragraphs</h3>
                {
                    data.fieldContentHeaderParagraph &&
                    <div className="header-paragraph-content">
                        <Paragraph paragraph={data.fieldContentHeaderParagraph.entity} />
                    </div>
                }
                {
                    data.fieldContentParagraph &&
                    <div className="content-paragraph-content">
                        {data.fieldContentParagraph.map((paragraph, index) => (
                            <Paragraph paragraph={paragraph.entity} key={index}/>
                        ))}
                    </div>
                }
            </div>
        </div>
    </div>
);

export default NodeContent;