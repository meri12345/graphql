import React from 'react';
import { Paragraph } from '../Paragraph';
import styles from './ParagraphLinkList.module.css';

const ParagraphLinkList = ({ item }) => (
  <div className={styles.list}>
    <p>{item.__typename}</p>
    <h3>Useful links</h3>
    {
      item.fieldLinks && (
        <div className={styles.list__links}>
          {item.fieldLinks.map((paragraph, index) => <Paragraph key={index} paragraph={paragraph.entity} />)}
        </div>
      )
    }
  </div>
);

export default ParagraphLinkList;