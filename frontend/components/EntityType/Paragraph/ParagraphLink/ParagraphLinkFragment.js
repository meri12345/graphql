import gql from 'graphql-tag';

export const ParagraphLinkFragment = gql`
  fragment ParagraphLink on ParagraphLink {
    fieldLink {
      title
      url {
        path
      }
    }
  }
`;