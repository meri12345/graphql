import React from 'react';
import ParagraphImage from './ParagraphImage/ParagraphImage';
import ParagraphLinkList from './ParagraphLinkList/ParagraphLinkList';
import ParagraphLink from './ParagraphLink/ParagraphLink';
import ParagraphText from './ParagraphTex/ParagraphText';

export const Paragraph = ({ paragraph }) => {
  switch (paragraph.__typename) {
    case 'ParagraphHeader':
      return (<div><p>{paragraph.__typename}</p>Paragraph header placeholder</div>);
    case 'ParagraphLinklist':
      return (<ParagraphLinkList item={paragraph} />);
    case 'ParagraphLink':
      return (<ParagraphLink item={paragraph} />);
    case 'ParagraphImage':
      return (<ParagraphImage item={paragraph} />);
    case 'ParagraphTex':
      return (<ParagraphText item={paragraph} />);
    default:
      return '';
  }
}