import gql from 'graphql-tag';

export const MAIN_MENU = gql`
    {
        menuByName(name: "main") {
            links {
                ... on MenuLink {
                    label
                    url {  
                        path
                    }
                }
            }
        }
    }
`;

