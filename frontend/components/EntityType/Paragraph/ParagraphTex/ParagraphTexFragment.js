import gql from 'graphql-tag';

export const ParagraphTexFragment = gql`
  fragment ParagraphTex on ParagraphTex {
    fieldText {
      value
    }
  }
`;