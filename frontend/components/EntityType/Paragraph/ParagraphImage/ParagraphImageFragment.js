import gql from 'graphql-tag';

export const ParagraphImageFragment = gql`
  fragment ParagraphImage on ParagraphImage {
    fieldImage {
      url
      alt
      title
    }
  }
`;