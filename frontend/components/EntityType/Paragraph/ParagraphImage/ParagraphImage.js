import React from 'react';
import styles from './ParagraphImage.module.css';

const ParagraphImage = ({ item }) => (
  <div className={styles.paragraph}>
    <p>{item.__typename}</p>
    <img src={item.fieldImage.url} alt={item.fieldImage.alt} />
  </div>
);

export default ParagraphImage;