import gql from 'graphql-tag';

export const ParagraphLinkListFragment = gql`
  fragment ParagraphLinkList on ParagraphLinklist {
    fieldLinks {
      entity {
        ...ParagraphLink
      }
    }
  }
`;