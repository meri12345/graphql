<?php

declare(strict_types = 1);

namespace Drupal\graphql_schema\Wrappers\Response;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\graphql\GraphQL\Response\Response;

/**
 * Type of response used when a content is returned.
 */
class ContentDeleteResponse extends Response {

  /**
   * The response to be served.
   *
   * @var string
   */
  protected $response;

  /**
   * Sets the content.
   *
   * @param string $response
   *   The content to be served.
   */
  public function setResponse(string $response): void {
    $this->response = $response;
  }

  /**
   * Gets the content to be served.
   *
   * @return string|null
   *   The content to be served.
   */
  public function reponse() {
    return $this->response;
  }

}
