<?php

namespace Drupal\graphql_schema\Plugin\GraphQL\DataProducer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql_schema\GraphQL\Buffers\EntityBundleBuffer;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use GraphQL\Deferred;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Load multiple entities by IDs.
 *
 * @DataProducer(
 *   id = "entity_load_by_bundle",
 *   name = @Translation("Load multiple entities by byndle"),
 *   description = @Translation("Loads multiple entities by bundle."),
 *   produces = @ContextDefinition("entities",
 *     label = @Translation("Entities")
 *   ),
 *   consumes = {
 *     "type" = @ContextDefinition("string",
 *       label = @Translation("Entity type")
 *     ),
 *     "bundles" = @ContextDefinition("string",
 *       label = @Translation("Entity bundle"),
 *     )
 *   }
 * )
 */
class EntityLoadByBundle extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity buffer service.
   *
   * @var \Drupal\graphql_schema\GraphQL\Buffers\EntityBundleBuffer
   */
  protected $entityBuffer;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('graphql.buffer.entity_by_bundle')
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\graphql_schema\GraphQL\Buffers\EntityBundleBuffer $entityBuffer
   *   The entity buffer service.
   *
   * @codeCoverageIgnore
   */
  public function __construct(
    array $configuration,
          $pluginId,
    array $pluginDefinition,
    EntityBundleBuffer $entityBuffer
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityBuffer = $entityBuffer;
  }

  /**
   * Resolver.
   *
   * @param string $type
   * @param string $bundle
   *
   * @return \GraphQL\Deferred
   */
  public function resolve($type, string $bundle) {
    $resolver = $this->entityBuffer->add($type, $bundle);

    return new Deferred(function () use ( $resolver) {
      /** @var \Drupal\Core\Entity\EntityInterface[] $entities */
      $entities = $resolver();

      return $entities;
    });
  }

}
