import Menu from "../components/Menu";
import React from "react";
import {withApollo} from "../libs/apollo";

function HomePage() {
  return (
    <>
      <Menu />
      <div>
          Welcome to Next.js!
      </div>
    </>
  );
}

export default withApollo({ ssr: true })(HomePage);