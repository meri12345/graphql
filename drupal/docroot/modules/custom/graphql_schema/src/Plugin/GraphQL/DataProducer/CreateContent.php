<?php

namespace Drupal\graphql_schema\Plugin\GraphQL\DataProducer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\graphql_schema\GraphQL\Response\ContentResponse;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates a new content entity.
 *
 * @DataProducer(
 *   id = "create_content",
 *   name = @Translation("Create Content"),
 *   description = @Translation("Creates a new content."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Content")
 *   ),
 *   consumes = {
 *     "data" = @ContextDefinition("any",
 *       label = @Translation("Content data")
 *     )
 *   }
 * )
 */
class CreateContent extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * CreateArticle constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
  }

  /**
   * Creates an article.
   *
   * @param array $data
   *   The title of the job.
   *
   * @return \Drupal\graphql_schema\GraphQL\Response\ContentResponse
   *   The newly created content.
   *
   * @throws \Exception
   */
  public function resolve(array $data) {
    $response = new ContentResponse();
    if ($this->currentUser->hasPermission("create content content")) {
      $paragraphs = [];

      foreach ($data['linkList'] as $link) {
        $paragraph = Paragraph::create([
          'type' => 'link',
          'field_link' => array(
            'uri' => $link['url'],
            'title' => $link['title'],
            "options" => array(
              "attributes" => array(
              "target" => "_blank"
         )))
        ]);
        $paragraph->save();
        $id = $paragraph->id();
        $revision = $paragraph->getRevisionId();
        $paragraphItem = array(
          'target_id' => $id,
          'target_revision_id' => $revision
        );
        array_push($paragraphs,$paragraphItem);
      }

      foreach ($data['textList'] as $text) {
        $paragraph = Paragraph::create([
          'type' => 'tex',
          'field_text' => $text['text']
        ]);
        $paragraph->save();
        $id = $paragraph->id();
        $revision = $paragraph->getRevisionId();
        $paragraphItem = array(
          'target_id' => $id,
          'target_revision_id' => $revision
        );
        array_push($paragraphs,$paragraphItem);
      }
      foreach ($data['imageList'] as $image) {
        $paragraph = Paragraph::create([
          'type' => 'image',
          'field_image' => array(
            'target_id' => $image['target_id'],
            'alt' => $image['alt'],
          )
        ]);
        $paragraph->save();
        $id = $paragraph->id();
        $revision = $paragraph->getRevisionId();
        $paragraphItem = array(
          'target_id' => $id,
          'target_revision_id' => $revision
        );
        array_push($paragraphs, $paragraphItem);
      }
      $paragraphHeader = [];
      if ($data['header']) {
        $paragraph = Paragraph::create([
          'type' => 'header',
          'field_image' => array(
            'target_id' => $data['header']['target_id'],
            'alt' => $data['header']['alt'],
          )
        ]);
        $paragraph->save();
        $id = $paragraph->id();
        $revision = $paragraph->getRevisionId();
        $paragraphHeader = array(
          'target_id' => $id,
          'target_revision_id' => $revision
        );
      }

      $values = [
        'type' => 'content',
        'title' => $data['title'],
        'field_summary' => $data['summary'],
        'field_content_paragraph' => $paragraphs,
        'field_content_header_paragraph' => $paragraphHeader
      ];
      $node = Node::create($values);
      $node->save();
      $response->setContent($node);
    }
    else {
      $response->addViolation(
        $this->t('You do not have permissions to create content.')
      );
    }
    return $response;
  }

}
