import gql from 'graphql-tag';

export const ARTICLES = gql`
    {
        nodeQuery(limit: 10, offset: 0, filter: {conditions: [{operator: EQUAL, field: "type", value: ["article"]}]}) {
            entities {
                ... on NodeArticle {
                    nid
                    title
                    body {
                        value
                    }
                }
            }
        }
    }
`;

