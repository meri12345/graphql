import React from 'react';

const ParagraphText = ({ item }) => (
    <div dangerouslySetInnerHTML={{ __html: item.fieldText.value }} />
);

export default ParagraphText;