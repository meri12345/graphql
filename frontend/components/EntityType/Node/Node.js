import React from 'react';
import NodeContent from './NodeContent/NodeContent';

export const Node = ({ node }) => {
  switch (node.__typename) {
    case 'NodeContent':
      return (<NodeContent data={node} />);
    default:
      return '';
  }
}