import React from 'react';
import styles from '../styles/PageTitle.module.css';

export const PageTitle = () => {
  return (
    <div className="container">
      <div className="content">
        <div className={styles.content}>
          <h1>My Page Title</h1>
        </div>
      </div>
    </div>
  );
};
