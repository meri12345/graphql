import React from 'react';

const Header = ({ item }) => (
    <div>
        <h2>{item}</h2>
    </div>
);

export default Header;