import React from 'react';
import ArticleTeaser from "../../components/ArticleTeaser";
import {ARTICLES} from "../../gql/articles";
import { client } from "../../apolloClient";
import Menu from "../../components/Menu";
import {MAIN_MENU} from "../../gql/menu";

function Page(data) {
    return (
        <>
            <Menu key={"menu"} data={data.menu}/>
            <div className="App">
                <h1>This is rendered on backend</h1>
                <div className="App-body">
                    <ul>
                        {data.data.map(article =>
                            <li key={article.nid}>
                                <ArticleTeaser key={article.nid} article={article} />
                            </li>
                        )}
                    </ul>
                </div>
            </div>
        </>
    );
}

// This gets called on every request
export async function getServerSideProps() {
    let data = [];
    await client
        .query({
            query: ARTICLES
        })
        .then(result => result.data.nodeQuery.entities.forEach(function(item) {
            data.push({nid: item.nid,title: item.title, body: {value: item.body.value}});
        }));

    let menu = [];
    await client
        .query({
            query: MAIN_MENU
        })
        .then(result => result.data.menuByName.links.forEach(function(item) {
            menu.push({label: item.label, url: item.url});
        }));

    return { props: { data, menu } }
}

export default Page