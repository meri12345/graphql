<?php

namespace Drupal\graphql_schema\Plugin\GraphQL\DataProducer;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\graphql_schema\GraphQL\Response\ContentDeleteResponse;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates a new content entity.
 *
 * @DataProducer(
 *   id = "delete_content",
 *   name = @Translation("Create Content"),
 *   description = @Translation("Creates a new content."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Content")
 *   ),
 *   consumes = {
 *     "data" = @ContextDefinition("any",
 *       label = @Translation("Content data")
 *     )
 *   }
 * )
 */
class DeleteContent extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * CreateArticle constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(array $configuration, string $plugin_id, array $plugin_definition, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
  }

  /**
   * Creates an article.
   *
   * @param array $data
   *   The title of the job.
   *
   * @return \Drupal\graphql_schema\GraphQL\Response\ContentDeleteResponse
   *   The newly created content.
   *
   * @throws \Exception
   */
  public function resolve(array $data) {
    $response = new ContentDeleteResponse();
    if ($this->currentUser->hasPermission("delete content content")) {
      $storage_handler = \Drupal::entityTypeManager()->getStorage('node');
      $entities = $storage_handler->loadMultiple($data);
      $storage_handler->delete($entities);
      $response->setResponse(count($entities) . " entities deleted.");
    }
    else {
      $response->addViolation(
        $this->t('You do not have permissions to delete content.')
      );
    }
    return $response;
  }

}
