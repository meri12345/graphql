import React from 'react';
import MenuLink from "./MenuLink";
import styles from '../styles/Menu.module.css';

const Menu = (data) => {
    return (
        <div className="container">
          <div className={styles.content}>
            {data.data.map((menu) => (
              <MenuLink key={menu.label} menu={menu} />
            ))}
          </div>
        </div>
    );
};

export default Menu;