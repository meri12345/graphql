# Project setup

Demo presentation of decoupled Drupal 8.

Backend: Drupal 8
Frontend: ReactJS + GraphQL

### Prerequisites

- Composer
- Node.JS v12+
- npm v6+
- Docker with Docker Compose


### Starting the project

1. Run `cd ./drupal/docroot && composer install`
2. Return to project root and run `cd ./frontend && npm install`
3. Copy `next.config.example.js` and rename to `next.config.js`
4. Return to project root, copy `.env.example` and rename to `.env`
5. Run `./start.sh` to start docker containers
6. To stop containers run `./stop.sh`
7. Copy `drupal/docroot/sites/default/files/default.settings.php` and rename to `settings.php`
8. Copy `drupal/docroot/sites/default/files/default.settings.local.php` and rename to `settings.local.php`
9. From project root, login to drupal container `docker-compose exec drupal bash`
10. Inside container go to docroot folder `cd drupal/docroot`
11. Run `vendor/bin/drush si react_demo`
12. (optional) Run `vendor/bin/drush uli` to set your admin password
13. Exit the container with `exit`
14. drush -y config-set system.performance css.preprocess 0
15. drush -y config-set system.performance js.preprocess 0

Visit the frontend through `http://fe.drupal-react.localhost`
Visit the backend through `http://be.drupal-react.localhost`


### Viewing content

You can add some nodes of type `Content` to display them on the frontend. The
index page will display a list of those contents. 

Frontend will detect if a URL exists in Drupal, so setting any aliases on a
node in the backend can be accessed from the frontend.

Ex. if a node with id 1 exists and has a path alias `/my-first-node`, it can
be accessed through `http://fe.drupal-react.localhost/node/1` or through
`http://fe.drupal-react.localhost/my-first-node`.
