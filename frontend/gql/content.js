import gql from 'graphql-tag';
import { NodeContentFragment} from '../components/EntityType/Node/NodeContent/NodeContentFragment';
import { ParagraphHeaderFragment } from '../components/EntityType/Paragraph/ParagraphHeader/ParagraphHeaderFragment';
import { ParagraphImageFragment } from '../components/EntityType/Paragraph/ParagraphImage/ParagraphImageFragment';
import { ParagraphLinkListFragment } from '../components/EntityType/Paragraph/ParagraphLinkList/ParagraphLinkListFragment';
import { ParagraphTexFragment } from '../components/EntityType/Paragraph/ParagraphTex/ParagraphTexFragment';
import { ParagraphLinkFragment } from '../components/EntityType/Paragraph/ParagraphLink/ParagraphLinkFragment';
import { NodeContentTeaserFragment } from '../components/EntityType/Node/NodeContent/Teaser/NodeContentTeaserFragment';

export const getContent = (param) => (gql`
  {
    entityByRoute: entityByRoute(path: "${param}") {
      ...NodeContentFragment
    }
  }
  ${NodeContentFragment}
  ${ParagraphHeaderFragment}
  ${ParagraphImageFragment}
  ${ParagraphLinkFragment}
  ${ParagraphLinkListFragment}
  ${ParagraphTexFragment}
  `
);

export const getContentTeasers = () => gql`
  {
    nodeQuery(limit: 20, offset: 0, filter: {conditions: [{operator: EQUAL, field: "type", value: ["content"]}]}) {
      entities {
        ...NodeContentTeaser
      }
    }
  }
  ${NodeContentTeaserFragment}
  ${ParagraphHeaderFragment}
`;

export const paragraphTypes = {
    image_paragraph: 'ParagraphImage',
    link_list_paragraph: 'ParagraphLinklist',
    text_paragraph: 'ParagraphTex'
};

export const ParagraphImage = {
    type: paragraphTypes.image_paragraph,
    url: '',
};

export const ParagraphLinkList = {
    type: paragraphTypes.link_list_paragraph,
    links: [],
};

export const ParagraphText = {
    type: paragraphTypes.text_paragraph,
    text: '',
};