import gql from 'graphql-tag';

export const ParagraphHeaderFragment = gql`
  fragment ParagraphHeader on ParagraphHeader {
    fieldImage {
      url
      alt
      title
    }
  }
`;