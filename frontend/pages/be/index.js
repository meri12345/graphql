import React from 'react';
import { ApolloClient, ApolloLink, concat } from "apollo-boost";
import gql from "graphql-tag";
import { ApolloProvider, Query } from "react-apollo";
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import ArticleTeaser from "../../components/ArticleTeaser";

function Page(data) {
  let paragraphs = data.data;
  let items = [];

  paragraphs.forEach(function(paragraph){
    if(paragraph.type === 'fieldImage') {
      items.push(<h2>Image</h2>);
      items.push(<img src={paragraph.url}/>);
    }

    if(paragraph.type === 'linklist') {
      items.push(<h2>Links</h2>);
      paragraph.links.forEach(function(link) {
        items.push(<p><a target="_blank" href={link.path}> {link.title} </a></p>)
      });
    }
  });

  return (
    <div className="App">
      <h1> {data.title} </h1>
      <div dangerouslySetInnerHTML={{ __html: data.body }} />
      {items}
    </div>
  );
}

const link = createHttpLink({
  uri: "http://drupal/graphql"
});

const client = new ApolloClient({
  ssrMode: true,
  link,
  cache: new InMemoryCache()
});

// This gets called on every request
export async function getServerSideProps() {
  let data = [];
  let title;
  let body;

  await client
  .query({
    fetchPolicy: 'no-cache',
    query: gql`
        {
            nodeQuery(limit: 10, offset: 0, filter: {conditions: [{operator: EQUAL, field: "type", value: ["content"]}]}) {
                entities {
                    ... on NodeContent {
                        title
                        body {
                            value
                        }
                        fieldContentParagraph {
                            ... on FieldNodeContentFieldContentParagraph {
                                entity {
                                    ... on ParagraphImage {
                                        fieldImage {
                                            url
                                            alt
                                            title
                                        }
                                    }
                                }
                            }
                        }
                        fieldContentParagraph {
                            ... on FieldNodeContentFieldContentParagraph {
                                entity {
                                    ... on ParagraphLinklist {
                                        fieldLinks {
                                            ... on FieldParagraphLinklistFieldLinks {
                                                entity {
                                                    ... on ParagraphLink {
                                                        fieldLink {
                                                            title
                                                            url {
                                                                path
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    `
  })
  .then(result => result.data.nodeQuery.entities.forEach(function(item) {
    title = item.title;
    body = item.body.value;
    item.fieldContentParagraph.forEach(function(paragraph) {
      if(paragraph.entity.hasOwnProperty('fieldImage')) {
        data.push({type: 'fieldImage', url: paragraph.entity.fieldImage.url});
      }
      if(paragraph.entity.hasOwnProperty('fieldLinks')) {
        let links = [];
        paragraph.entity.fieldLinks.forEach(function(link) {
          links.push({
            title: link.entity.fieldLink.title,
            path: link.entity.fieldLink.url.path
          })
        });
        data.push({ type: 'linklist', links: links });
      }
    })
  }));
  if(title === undefined) {
    title = '';
  }
  if(body === undefined) {
    body = '';
  }
  return { props: { title, body, data } }
}

export default Page