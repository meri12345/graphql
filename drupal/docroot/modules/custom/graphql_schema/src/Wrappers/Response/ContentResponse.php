<?php

declare(strict_types = 1);

namespace Drupal\graphql_schema\Wrappers\Response;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\graphql\GraphQL\Response\Response;

/**
 * Type of response used when a content is returned.
 */
class ContentResponse extends Response {

  /**
   * The content to be served.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface|null
   */
  protected $content;

  /**
   * Sets the content.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $content
   *   The content to be served.
   */
  public function setContent(?ContentEntityInterface $content): void {
    $this->content = $content;
  }

  /**
   * Gets the content to be served.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The content to be served.
   */
  public function content(): ?ContentEntityInterface {
    return $this->content;
  }

}
