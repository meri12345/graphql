import gql from 'graphql-tag';

export const NodeContentTeaserFragment = gql`
  fragment NodeContentTeaser on NodeContent {
    nid
    title
    entityUrl {
      path
    }
    entityCreated
    uid {
      entity {
        entityLabel
      }
    }
    fieldSummary
    fieldContentHeaderParagraph {
      entity {
        ...ParagraphHeader
      }
    }
  }
`;