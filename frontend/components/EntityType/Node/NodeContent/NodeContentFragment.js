import gql from 'graphql-tag';

export const NodeContentFragment = gql`
  fragment NodeContentFragment on NodeContent {
    title
    fieldSummary
    fieldContentHeaderParagraph {
      entity {
        ...ParagraphHeader
      }
    }
    fieldContentParagraph {
      entity {
        ...ParagraphImage
        ...ParagraphLinkList
        ...ParagraphTex
      }
    }
  }
`;

