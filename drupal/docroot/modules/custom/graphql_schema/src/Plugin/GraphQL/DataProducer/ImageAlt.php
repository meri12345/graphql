<?php

namespace Drupal\graphql_schema\Plugin\GraphQL\DataProducer;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Returns an image style derivative of an image.
 *
 * @DataProducer(
 *   id = "image_alt",
 *   name = @Translation("Image Alternative Text"),
 *   description = @Translation("Returns an image alt."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Image alt")
 *   ),
 *   consumes = {
 *     "paragraph" = @ContextDefinition("entity",
 *       label = @Translation("Paragraph")
 *     )
 *   }
 * )
 */
class ImageAlt extends DataProducerPluginBase {

  /**
   * Resolver.
   *
   * @param \Drupal\paragraphs\Entity\Paragraph  $paragraph
   *
   * @return string
   */
  public function resolve($paragraph) {
    return $paragraph->get('field_image')->getValue()[0]['alt'];
  }
}
