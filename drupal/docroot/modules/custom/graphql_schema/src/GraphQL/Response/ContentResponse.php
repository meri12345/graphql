<?php

namespace Drupal\graphql_schema\GraphQL\Response;

use Drupal\Core\Entity\EntityInterface;
use Drupal\graphql\GraphQL\Response\Response;

/**
 * Type of response used when a content is returned.
 */
class ContentResponse extends Response {

  /**
   * The content to be served.
   *
   * @var \Drupal\Core\Entity\EntityInterface|null
   */
  protected $content;

  /**
   * Sets the content.
   *
   * @param \Drupal\Core\Entity\EntityInterface|null $content
   *   The content to be served.
   */
  public function setContent(?EntityInterface $content): void {
    $this->content = $content;
  }

  /**
   * Gets the content to be served.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The content to be served.
   */
  public function content(): ?EntityInterface {
    return $this->content;
  }

}
