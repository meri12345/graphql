import React from 'react';
import ArticleTeaser from '../../components/ArticleTeaser';
import {ARTICLES} from "../../gql/articles";
import {withApollo} from "../../libs/apollo";
import {useQuery} from "@apollo/react-hooks";
import Menu from "../../components/Menu";
import {MAIN_MENU} from "../../gql/menu";

const ArticlesView = () => {
    const {loading, error, data} = useQuery(ARTICLES);
    if (error) return <h1>Error</h1>;
    if (loading) return <h1>Loading...</h1>;

    return (
        <ul>
            {data.nodeQuery.entities.map(article =>
                <li key={article.nid}>
                    <ArticleTeaser article={article}/>
                </li>
            )}
        </ul>
    )
};

function App() {
    const { loading, error, data } = useQuery(MAIN_MENU);
    if (error) return <h1>Error</h1>;
    if (loading) return <h1>Loading...</h1>;

    return (
        <>
            <Menu data={data.menuByName.links}/>
            <div className="App">
                <h1>This is rendered on frontend</h1>
                <div className="App-body">
                    <ArticlesView />
                </div>
            </div>
        </>
    );
}

export default withApollo({ ssr: true })(App);
