<?php

namespace Drupal\graphql_schema\Plugin\GraphQL\SchemaExtension;

use Drupal\Core\Utility\Error;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\GraphQL\Response\ResponseInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\graphql_schema\GraphQL\Response\ArticleResponse;
use Drupal\graphql_schema\GraphQL\Response\ContentResponse;
use Drupal\graphql_schema\GraphQL\Response\ContentDeleteResponse;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * @SchemaExtension(
 *   id = "composable_extension",
 *   name = "Composable Example extension",
 *   description = "A simple extension that adds node related fields.",
 *   schema = "composable"
 * )
 */
class ComposableSchemaExampleExtension extends SdlSchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver('Query', 'article',
      $builder->produce('entity_load')
        ->map('type', $builder->fromValue('node'))
        ->map('bundles', $builder->fromValue(['article']))
        ->map('id', $builder->fromArgument('id'))
    );

    // Create article mutation.
    $registry->addFieldResolver('Mutation', 'createArticle',
      $builder->produce('create_article')
        ->map('data', $builder->fromArgument('data'))
    );

    $registry->addFieldResolver('Mutation', 'createContent',
      $builder->produce('create_content')
      ->map('data', $builder->fromArgument('data'))
    );

    $registry->addFieldResolver('Mutation', 'deleteContent',
      $builder->produce('delete_content')
        ->map('data', $builder->fromArgument('data'))
    );

    $registry->addFieldResolver('ContentResponse', 'content',
      $builder->callback(function (ContentResponse $response) {
        return $response->content();
      })
    );

    $registry->addFieldResolver('ContentResponse', 'errors',
      $builder->callback(function (ContentResponse $response) {
        return $response->getViolations();
      })
    );

    $registry->addFieldResolver('ContentDeleteResponse', 'response',
      $builder->callback(function (ContentDeleteResponse $response) {
        return $response->response();
      })
    );

    $registry->addFieldResolver('ContentDeleteResponse', 'errors',
      $builder->callback(function (ContentDeleteResponse $response) {
        return $response->getViolations();
      })
    );

    $registry->addFieldResolver('ArticleResponse', 'article',
      $builder->callback(function (ArticleResponse $response) {
        return $response->article();
      })
    );

    $registry->addFieldResolver('ArticleResponse', 'errors',
      $builder->callback(function (ArticleResponse $response) {
        return $response->getViolations();
      })
    );

    $registry->addFieldResolver('Article', 'id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('Article', 'title',
      $builder->compose(
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('Article', 'author',
      $builder->compose(
        $builder->produce('entity_owner')
          ->map('entity', $builder->fromParent()),
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('Query', 'content',
      $builder->produce('entity_load')
        ->map('type', $builder->fromValue('node'))
        ->map('bundles', $builder->fromValue(['content']))
        ->map('id', $builder->fromArgument('id'))
    );

    $registry->addFieldResolver('Query', 'nodesByBundle',
      $builder->produce('entity_load_by_bundle')
        ->map('type', $builder->fromValue('node'))
        ->map('bundles', $builder->fromArgument('bundle'))
    );

    $registry->addFieldResolver('Content', 'id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('Content', 'title',
      $builder->compose(
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('Content', 'author',
      $builder->compose(
        $builder->produce('entity_owner')
          ->map('entity', $builder->fromParent()),
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('Content', 'summary',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:node'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_summary.value'))
    );

    $registry->addFieldResolver('Content', 'header',
      $builder->produce('entity_reference_revisions')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_content_header_paragraph'))
    );

    $registry->addFieldResolver('Image', 'alt',
      $builder->compose(
        $builder->produce('image_alt')
          ->map('paragraph',$builder->fromParent())
      )
    );

    $registry->addFieldResolver('Image', 'url',
      $builder->compose(
        $builder->produce('image_url')
          ->map('paragraph',$builder->fromParent())
          ->map('style',$builder->fromValue('thumbnail'))
      )
    );

    $registry->addFieldResolver('HeaderImage', 'alt',
      $builder->compose(
        $builder->produce('image_alt')
          ->map('paragraph',$builder->fromParent())
      )
    );

    $registry->addFieldResolver('HeaderImage', 'url',
      $builder->compose(
        $builder->produce('image_url')
          ->map('paragraph',$builder->fromParent())
          ->map('style',$builder->fromValue('thumbnail'))
      )
    );

    $registry->addFieldResolver('Text', 'text',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:paragraph'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('field_text.value'))
    );

    $registry->addFieldResolver('Content', 'content',
      $builder->produce('entity_reference_revisions')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_content_paragraph'))
    );

    $registry->addFieldResolver('LinkList', 'links',
      $builder->produce('entity_reference_revisions')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_links'))
    );

    $registry->addFieldResolver('Link', 'title',
      $builder->compose(
        $builder->produce('link_attr')
          ->map('paragraph',$builder->fromParent())
          ->map('attribute',$builder->fromValue('title'))
      )
    );

    $registry->addFieldResolver('Link', 'url',
      $builder->compose(
        $builder->produce('link_attr')
          ->map('paragraph',$builder->fromParent())
          ->map('attribute',$builder->fromValue('uri'))
      )
    );

    $registry->addTypeResolver('Paragraph', function ($value) {
      if ($value instanceof Paragraph) {
        switch ($value->bundle()) {
          case "image": return "Image";
          case "tex": return "Text";
          case "header": return "HeaderImage";
          case "linklist": return "LinkList";
        }
      }
    });

    $registry->addTypeResolver('Node', function ($value) {
      if ($value instanceof Node) {
        switch ($value->bundle()) {
          case "content": return "Content";
          case "article": return "Article";
        }
      }
    });

    // Response type resolver.
    $registry->addTypeResolver('Response', [
      __CLASS__,
      'resolveResponse',
    ]);
  }

  /**
   * Resolves the response type.
   *
   * @param \Drupal\graphql\GraphQL\Response\ResponseInterface $response
   *   Response object.
   *
   * @return string
   *   Response type.
   *
   * @throws \Exception
   *   Invalid response type.
   */
  public static function resolveResponse(ResponseInterface $response): string {
    // Resolve content response.
    if ($response instanceof ArticleResponse) {
      return 'ArticleResponse';
    }
    throw new \Exception('Invalid response type.');
  }

}
