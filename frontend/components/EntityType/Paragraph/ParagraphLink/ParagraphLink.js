import React from 'react';
import styles from './ParagraphLink.module.css';

export const ParagraphLink = ({ item }) => (
  <>
    <p>{item.__typename}</p>
    <a
      className={styles.link}
      target="_blank"
      href={item.fieldLink.url.path}
    >{item.fieldLink.title}</a>
  </>
);

export default ParagraphLink;