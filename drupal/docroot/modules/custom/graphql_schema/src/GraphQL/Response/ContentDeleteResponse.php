<?php

namespace Drupal\graphql_schema\GraphQL\Response;

use Drupal\graphql\GraphQL\Response\Response;

/**
 * Type of response used when a content is returned.
 */
class ContentDeleteResponse extends Response {

  /**
   * The response to be served.
   *
   * @var string
   */
  protected $response;

  /**
   * Sets the content.
   *
   * @param string $response
   *   The content to be served.
   */
  public function setResponse(string $response): void {
    $this->response = $response;
  }

  /**
   * Gets the reponse to be served.
   *
   * @return string|null
   *   The reponse to be served.
   */
  public function response() {
    return $this->response;
  }

}
