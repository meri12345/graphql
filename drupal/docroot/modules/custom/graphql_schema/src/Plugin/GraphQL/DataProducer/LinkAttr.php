<?php

namespace Drupal\graphql_schema\Plugin\GraphQL\DataProducer;

use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Returns text for link paragraph.
 *
 * @DataProducer(
 *   id = "link_attr",
 *   name = @Translation("Link Attributes"),
 *   description = @Translation("Returns link attributes."),
 *   produces = @ContextDefinition("string",
 *     label = @Translation("Link attributes")
 *   ),
 *   consumes = {
 *     "paragraph" = @ContextDefinition("entity",
 *       label = @Translation("Paragraph")
 *     ),
 *     "attribute" = @ContextDefinition("string",
 *       label = @Translation("Atribute")
 *     )
 *   }
 * )
 */
class LinkAttr extends DataProducerPluginBase {

  /**
   * Resolver.
   *
   * @param \Drupal\paragraphs\Entity\Paragraph  $paragraph
   * @param string $attribute
   *
   * @return string
   */
  public function resolve(Paragraph $paragraph, $attribute) {
    return $paragraph->get('field_link')->getValue()[0][$attribute];
  }
}
