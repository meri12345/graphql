import React from 'react';
import { client } from '../apolloClient';
import Menu from "../components/Menu";
import {MAIN_MENU} from "../gql/menu";
import { getContentTeasers } from "../gql/content";
import { PageTitle } from '../components/PageTitle';
import { NodeContentTeaser } from '../components/EntityType/Node/NodeContent/Teaser/NodeContentTeaser';

const IndexPage = ({ menu, teasers }) => {
    return (
        <div className="page">
          <header className="content-center">
            <PageTitle />
            <Menu data={menu} />
          </header>
          <div className="body content-center">
            <div className="container">
              <h2>List of Content nodes</h2>
              { teasers.entities && teasers.entities.map((teaser, index) => (teaser && <NodeContentTeaser key={index} content={teaser} />) )}
              { !teasers.entities && <h3>Currently there is no content</h3> }
            </div>
          </div>
        </div>
    );
};

export async function getServerSideProps() {
  let teasers = null;

  await client.query({
    query: getContentTeasers(),
    fetchPolicy: 'no-cache',
  })
    .then((result) => {
      teasers = result.data.nodeQuery;
    });

  let menu = [];
  await client.query({
    query: MAIN_MENU,
  })
    .then((result) => {
      result.data.menuByName.links.forEach(function (item) {
        menu.push({
          label: item.label,
          url: item.url
        });
      });
    });

  return {
    props: {
      menu,
      teasers,
    },
  };
}

export default IndexPage;