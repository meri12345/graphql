import React from 'react';
import styles from './NodeContentTeaser.module.css';

const trimText = (string) => {
  return string.length > 400 ? string.substring(0, 397) + "..." : string;
}

export const NodeContentTeaser = ({ content }) => (
  <div className={styles.teaser}>
    <h4 className="teaser__title">
      <a
        href={content.entityUrl.path}
        className={styles.teaser__link}
      >{content.title}</a>
    </h4>
    <div className={styles.teaser__info}>
      Created: {content.entityCreated} Author: {content.uid.entity.entityLabel}
    </div>
    <p>{trimText(content.fieldSummary)}</p>
  </div>
);

export default NodeContentTeaser;