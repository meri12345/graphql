import React from 'react';
import styles from '../styles/MenuLink.module.css'

const MenuLink = ({ menu }) => (
  <a
    className={styles.link}
    href={menu.url.path}
  >
    {menu.label}
  </a>
);

export default MenuLink;